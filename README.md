![alt-text](https://i.ibb.co/vd34Rp2/logo-kotiles-hitam-min.png)

Final Project CSCM602023 Advanced Programming @ Faculty of Computer Science Universitas Indonesia, Term 2 2019/2020

### Branch Status
**master** :
[![pipeline status](https://gitlab.com/iqrar99/kotiles/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/master)
[![coverage report](https://gitlab.com/iqrar99/kotiles/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/master)
* iqrar :
[![pipeline status](https://gitlab.com/iqrar99/kotiles/badges/iqrar/pipeline.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/iqrar)
[![coverage report](https://gitlab.com/iqrar99/kotiles/badges/iqrar/coverage.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/iqrar)
* bonar :
[![pipeline status](https://gitlab.com/iqrar99/kotiles/badges/bonar/pipeline.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/bonar)
[![coverage report](https://gitlab.com/iqrar99/kotiles/badges/bonar/coverage.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/bonar)
* fadhlan :
[![pipeline status](https://gitlab.com/iqrar99/kotiles/badges/fadhlan/pipeline.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/fadhlan)
[![coverage report](https://gitlab.com/iqrar99/kotiles/badges/fadhlan/coverage.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/fadhlan)
* ariq :
[![pipeline status](https://gitlab.com/iqrar99/kotiles/badges/ariq/pipeline.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/ariq)
[![coverage report](https://gitlab.com/iqrar99/kotiles/badges/ariq/coverage.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/ariq)
* farrel :
[![pipeline status](https://gitlab.com/iqrar99/kotiles/badges/abi/pipeline.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/abi)
[![coverage report](https://gitlab.com/iqrar99/kotiles/badges/abi/coverage.svg?style=flat-square)](https://gitlab.com/iqrar99/kotiles/-/commits/abi)

## OUR TEAM
1. Bonar Wilfred &emsp;&emsp;&emsp;&emsp; (1806205451)
2. Farrel Alfarabi Saleh &emsp;&nbsp; (1806186622)
3. Iqrar Agalosi Nureyza &nbsp;&nbsp; (1806204902)
4. Muhammad Fadhlan &emsp; (1806205060)
5. Ariq Munif Kusuma &emsp;&nbsp;&nbsp; (1706039692)

## DESCRIPTION
Kotiles (Kotak Tiles)  is a web based solo puzzle game inspired from Klotski puzzle and 15-puzzle. This project is aiming to entertain users. This project will use Java Spring as the backend and React.js as the frontend.  It will be deployed on heroku or netlify.

## GAME FLOW
![alt-text](https://i.ibb.co/107vDPM/1.png)