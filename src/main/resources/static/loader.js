var scripts = [
    "main",
    "play",
    "jquery-3.5.1.min"
]

var styles = [
    "main",
    "fonts",
    "section",
    "navbar",
    "imageContainer"
]

// TODO: refactor loaders
function loadScript() {
    for (script of scripts) appendScript(script);
}

function loadStyle() {
    for (style of styles) appendStyle(style, null);
}

// TODO: refactor appends and probably find some "Thymeleaf compliant" solutions
function appendScript(url) {
    var head = document.getElementsByTagName("head")[0];
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = `/res/js/${url}.js`;
    head.appendChild(script);
}

function appendStyle(url, styleId) {
    var head = document.getElementsByTagName("head")[0];
    var style = document.createElement("link");
    style.rel = "stylesheet";
    style.type = "text/css";
    style.href = `/res/css/${url}.css`;
    if (styleId != null) style.id = styleId;
    head.appendChild(style);
}

loadScript();
loadStyle();