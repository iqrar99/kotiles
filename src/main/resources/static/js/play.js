function swapTilesCmp(row, col) {
    console.log(row, col);

    $.ajax({
        type: "POST",
        url: location.origin + "/play/competitive/",
        headers: {"Access-Control-Allow-Origin" : "*"},
        data: {
            row : row,
            col : col,
        },
        success: function () {
            console.log("Data sukses dikirim ke Spring Boot");
        },
        complete: function () {
            $("#board").load(location.href+" #board>*", "");
        }
    });
}

// ----- TIMER SECTION -----
var h2 = document.getElementsByTagName('h2')[0],
    start = document.getElementById('start'),
    stop = document.getElementById('stop'),
    seconds = 0,
    minutes = 0,
    hours = 0,
    t,
    timeResult;

function add() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }
    h2.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

    timer();
}

function timer() {
    t = setTimeout(add, 1000);
    stop.disabled = false;
    start.disabled = true;
}

start.onclick = timer;
stop.onclick = function () {
    clearTimeout(t);
    timeResult = h2.textContent;

    var delay = window.setTimeout(function () {
        window.location = "/play/game-info";
    }, 1000);

    alert("Your Time: "+timeResult);
}
// ----- END OF TIMER -----