var themes = [
    "default",
    "dark"
]

function loadTheme(index) {
    var theme = themes[index];
    var themeUrl = document.getElementById("theme-url");

    if(themeUrl == null) appendStyle(`themes/${theme}`, "theme-url");
    else themeUrl.href = `/res/css/themes/${theme}.css`;
}

window.onload = function() {
    var themeButton = document.getElementById("theme-button");
    var themeCounter = 0;

    themeButton.onclick = function() {
        themeCounter++;
        if (themeCounter == themes.length) themeCounter = 0;

        loadTheme(themeCounter);
    }

    loadTheme(themeCounter);
}