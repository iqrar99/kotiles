package com.kotiles.project.core.gameplay;

import com.kotiles.project.service.BoardActImpl;

public class Board {
    private int level;
    private Tile[][] tilesGrid;
    private int boardSize;
    private BoardActImpl boardAct = new BoardActImpl();


    /**
     * Game preparation method to initialize all tile elements.
     */
    public void setUp() {
        setBoardSize();
        tilesGrid = new Tile[boardSize][boardSize];
        int counter = 0;

        for (int row = 0; row < boardSize; row++) {
            for (int col = 0; col < boardSize; col++) {
                tilesGrid[row][col] = new Tile();
                Tile tile = tilesGrid[row][col];
                tile.setTileId(counter++);
                tile.setRowPosition(row);
                tile.setColPosition(col);
            }
        }
    }

    /**
     * A method to start the game. After setting some game preparation,
     * this method will be run.
     */
    public void start(int level) {
        setLevel(level);
        setUp();
        setTilesGrid(shuffleTheBoard());
    }

    /**
     * A method to swap two tiles between empty and non empty tile.
     * @param selectedTile : the tile that should be swapped with empty tile.
     */
    public void swapTiles(Tile selectedTile) {
        int row = selectedTile.getRowPosition();
        int col = selectedTile.getColPosition();
        setTilesGrid(boardAct.swap2Tiles(row, col, this));

        isCompleted();
    }

    /**
     * A method to check whether the user has completed the game or not.
     * @return boolean
     */
    public boolean isCompleted() {

        return boardAct.isFinished(this);
    }

    /**
     * A method to shuffle the tiles on the board before play.
     * @return shuffled tiles.
     */
    public Tile[][] shuffleTheBoard() {
        return boardAct.shuffle(this);
    }

    /**
     * A method to set the tiles grid.
     * @param tilesGrid : the tiles grid that need to be assigned.
     */
    public void setTilesGrid(Tile[][] tilesGrid) {
        this.tilesGrid = tilesGrid;
    }

    /**
     * A method to set the board size based on the game level.
     */
    public void setBoardSize() {
        this.boardSize = getLevel() + 2;
    }

    /**
     * A method to set the level game. This method will be performed
     * during game preparation.
     * @param level : game level.
     */
    public final void setLevel(int level) {
        this.level = level;
    }

    /**
     * A method to get current tiles grid from a board.
     * @return the tiles grid from a board.
     */
    public final Tile[][] getTilesGrid() {
        return tilesGrid;
    }

    /**
     * A method to get the board size. Since the board is a square,
     * we only need return the row or the column.
     * @return the board size.
     */
    public final int getBoardSize() {
        return boardSize;
    }

    /**
     * A method to get the current level.
     * @return current level.
     */
    public final int getLevel() {
        return level;
    }
}
