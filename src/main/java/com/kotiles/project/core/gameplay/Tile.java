package com.kotiles.project.core.gameplay;

public class Tile {
    int tileId;
    int rowPosition;
    int colPosition;

    public int getTileId() {
        return tileId;
    }

    public void setTileId(int tileId) {
        this.tileId = tileId;
    }

    public int getColPosition() {
        return colPosition;
    }

    public void setColPosition(int colPosition) {
        this.colPosition = colPosition;
    }

    public int getRowPosition() {
        return rowPosition;
    }

    public void setRowPosition(int rowPosition) {
        this.rowPosition = rowPosition;
    }

    /**
     * A method to duplicate this class to avoid pass by reference
     * when assigning an object into an object.
     *
     * @return duplicated object.
     */
    public Tile duplicate() {
        Tile newTile = new Tile();
        newTile.setTileId(this.tileId);
        newTile.setRowPosition(this.rowPosition);
        newTile.setColPosition(this.colPosition);

        return newTile;
    }
}
