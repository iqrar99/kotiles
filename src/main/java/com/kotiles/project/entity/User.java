package com.kotiles.project.entity;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String email;

    private int score;

    /**
     * user Model.
     *
     * @param id -  user id
     * @param name  - user name
     * @param email - user email
     * @param score - user score
     */
    public User(Long id, String name, String email, int score) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.score = score;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getscore() {
        return score;
    }

    public void setscore(int score) {
        this.score = score;
    }
}
