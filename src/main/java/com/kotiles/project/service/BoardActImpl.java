package com.kotiles.project.service;

import com.kotiles.project.core.gameplay.Board;
import com.kotiles.project.core.gameplay.Tile;
import java.util.Random;
import org.springframework.stereotype.Service;

@Service
public class BoardActImpl implements BoardAct {
    private Random random = new Random();

    @Override
    public Tile[][] shuffle(Board board) {
        int boardSize = board.getBoardSize();
        int randomRow;
        int randomCol;

        // shuffle 500 times
        for (int i = 0; i < 500; i++) {
            randomRow = random.nextInt(boardSize);
            randomCol = random.nextInt(boardSize);

            board.setTilesGrid(swap2Tiles(randomRow, randomCol, board));
        }

        return board.getTilesGrid();
    }

    @Override
    public Tile[][] swap2Tiles(int row, int col, Board board) {
        Tile[][] tilesGrid = board.getTilesGrid();
        Tile emptyTile = findEmptyTile(row, col, board);

        if (emptyTile != null) {
            int rowTarget = emptyTile.getRowPosition();
            int colTarget = emptyTile.getColPosition();

            Tile tileTarget = tilesGrid[rowTarget][colTarget].duplicate();
            tilesGrid[rowTarget][colTarget] = null;

            Tile tileSource = tilesGrid[row][col].duplicate();
            tilesGrid[row][col] = null;

            tilesGrid[row][col] = tileTarget;
            tilesGrid[rowTarget][colTarget] = tileSource;

            tilesGrid[row][col].setRowPosition(row);
            tilesGrid[row][col].setColPosition(col);

            tilesGrid[rowTarget][colTarget].setRowPosition(rowTarget);
            tilesGrid[rowTarget][colTarget].setColPosition(colTarget);
        }

        return tilesGrid;
    }

    @Override
    public Tile findEmptyTile(int row, int col, Board board) {
        Tile[][] tilesGrid = board.getTilesGrid();
        int[][] direction = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

        for (int i = 0; i < 4; i++) {
            try {
                Tile tileNow;
                tileNow = tilesGrid[row + direction[i][0]][col + direction[i][1]].duplicate();
                if (tileNow.getTileId() == 0) {
                    return tileNow;
                }
            } catch (Exception e) {
                // do nothing
            }
        }

        return null;
    }

    @Override
    public boolean isFinished(Board board) {
        Tile[][] tilesGrid = board.getTilesGrid();
        int counter = 0;
        int boardSize = board.getBoardSize();

        for (int row = 0; row < boardSize; row++) {
            for (int col = 0; col < boardSize; col++) {
                if (tilesGrid[row][col].getTileId() != counter) {
                    return false;
                } else {
                    counter++;
                }
            }
        }

        return true;
    }
}
