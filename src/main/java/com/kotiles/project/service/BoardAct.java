package com.kotiles.project.service;

import com.kotiles.project.core.gameplay.Board;
import com.kotiles.project.core.gameplay.Tile;

public interface BoardAct {
    Tile[][] shuffle(Board board);

    Tile[][] swap2Tiles(int row, int col, Board board);

    Tile findEmptyTile(int roe, int col, Board board);

    boolean isFinished(Board board);
}
