package com.kotiles.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

// Prototype controller. REMOVE AFTER APPLICATION IS MOSTLY COMPLETED.

@Controller
@RequestMapping(value = "/prototype")
public class PrototypeController {

    @GetMapping(value = "/home")
    private String homePrototype() {
        return "home";
    }

    @GetMapping(value = "/profile")
    public String profilePrototype() {
        return "profile";
    }

    @GetMapping(value = "/game")
    public String gamePrototype() {
        return "game";
    }

    @GetMapping(value = "/leaderboard")
    public String leaderboardPrototype() {
        return "leaderboard";
    }
}

