package com.kotiles.project.controller;

import com.kotiles.project.core.gameplay.Board;
import com.kotiles.project.core.gameplay.Tile;
import com.kotiles.project.service.BoardAct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/play")
public class PlayController {

    @Autowired
    private BoardAct boardAct;

    private Board board;
    private boolean startedCmp = false;
    private boolean startedCsl = false;

    @GetMapping(value = "")
    public String mainMenu() {
        return "home";
    }

    /**
     * Method to handle competitive page. This page contains some gameplay logics.
     * @param model : Object for passing attribute into thymeleaf.
     * @return page render.
     */
    @GetMapping(value = "/competitive")
    public String playCompetitive(Model model) {
        if (!startedCmp) {
            startedCmp = true;
            board = new Board();
            board.start(1);
        }

        model.addAttribute("boardSize", board.getBoardSize());
        model.addAttribute("tilesGrid", board.getTilesGrid());
        model.addAttribute("gameLevel", board.getLevel());
        model.addAttribute("gameType", "competitive");

        return "game";
    }

    /**
     * method to receive POST request after user clicks a tile.
     * @param row : row of tile that user clicked before.
     * @param col : column of tile that user clicked before.
     * @return redirect to origin gameplay page.
     */
    @PostMapping(value = "/competitive")
    public @ResponseBody
    RedirectView pressedTileCmp(@RequestParam("row") int row,
                                @RequestParam("col") int col) {

        Tile[][] tilesGrid = board.getTilesGrid();
        Tile selectedTile = tilesGrid[row][col];
        board.swapTiles(selectedTile);

        if (board.isCompleted()) {
            startedCmp = false;

            return new RedirectView("/play/game-info");
        }

        return new RedirectView("/play/competitive");
    }

    /**
     * Method to handle casual page. This page contains some gameplay logics.
     * @param model : Object for passing attribute into thymeleaf.
     * @return page render.
     */
    @GetMapping(value = "/casual")
    public String playCasual(Model model) {
        if (!startedCsl) {
            startedCsl = true;
            board = new Board();
            board.start(1);
        }

        model.addAttribute("boardSize", board.getBoardSize());
        model.addAttribute("tilesGrid", board.getTilesGrid());
        model.addAttribute("gameLevel", board.getLevel());
        model.addAttribute("gameType", "casual");

        return "game";
    }

    /**
     * method to receive POST request after user clicks a tile.
     * @param row : row of tile that user clicked before.
     * @param col : column of tile that user clicked before.
     * @return redirect to origin gameplay page.
     */
    @PostMapping(value = "/casual")
    public @ResponseBody
    RedirectView pressedTileCsl(@RequestParam("row") int row,
                          @RequestParam("col") int col) {

        Tile[][] tilesGrid = board.getTilesGrid();
        Tile selectedTile = tilesGrid[row][col];
        board.swapTiles(selectedTile);

        if (board.isCompleted()) {
            startedCmp = false;

            return new RedirectView("/play/game-info");
        }

        return new RedirectView("/play/casual");
    }

    /**
     * Method to shoe game result when player completed the game.
     * @param model some attributes that need to be showed.
     * @return page render.
     */
    @GetMapping(value = "/game-info")
    public String gameInfo(Model model) {
        return "game-info";
    }
}
