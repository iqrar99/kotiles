package com.kotiles.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class MainController {

    @GetMapping(value = "/")
    private String homepage() {
        return "home";
    }

    @GetMapping(value = "/profile")
    public String getProfile(Model model) {
        return "profile";
    }
}

