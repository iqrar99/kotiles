package com.kotiles.project.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class PlayControllerTest {
    @Autowired
    public MockMvc mockMvc;

    @Test
    public void whenMainMenuUrlAccessedItShouldRedirectToGoogleSignIn() throws Exception {
        mockMvc.perform(get("/play"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrlPattern("**/oauth2/authorization/google"));
    }

    @Test
    public void whenCompetitiveUrlAccessedItShouldRedirectToGoogleSignIn() throws Exception {
        mockMvc.perform(get("/play/competitive"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrlPattern("**/oauth2/authorization/google"));
    }

    @Test
    public void whenCasualUrlAccessedItShouldRedirectToGoogleSignIn() throws Exception {
        mockMvc.perform(get("/play/casual"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrlPattern("**/oauth2/authorization/google"));
    }
}
