package com.kotiles.project.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.kotiles.project.core.gameplay.Board;
import com.kotiles.project.core.gameplay.Tile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BoardActImplTest {
    BoardActImpl boardAct;
    Board board;

    @BeforeEach
    public void setUp() {
        boardAct = new BoardActImpl();
        board = new Board();
    }

    @Test
    public void testMethodShuffle() {
        board.setLevel(1);
        board.setUp();
        Tile[][] tilesGrid = boardAct.shuffle(board);
        boolean isShuffled = false;
        int count = 0;

        outerloop:
        for (int row = 0; row < board.getBoardSize(); row++) {
            for (int col = 0; col < board.getBoardSize(); col++) {
                Tile tmpTile = tilesGrid[row][col];
                if (tmpTile.getTileId() != count) {
                    isShuffled = true;
                    break outerloop;
                } else {
                    count++;
                }
            }
        }

        assertTrue(isShuffled);
    }

    @Test
    public void testMethodSwap2TilesSuccess() {
        board.setLevel(1);
        board.setUp();
        Tile[][] tmpTilesGrid = boardAct.swap2Tiles(1, 0, board);
        assertEquals(0, tmpTilesGrid[1][0].getTileId());
    }

    @Test
    public void testMethodSwap2TilesFailed() {
        board.setLevel(1);
        board.setUp();
        Tile[][] tmpTilesGrid = boardAct.swap2Tiles(0, 0, board);
        assertEquals(0, tmpTilesGrid[0][0].getTileId());

        tmpTilesGrid = boardAct.swap2Tiles(1, 1, board);
        assertEquals(4, tmpTilesGrid[1][1].getTileId());
    }

    @Test
    public void testMethodFindEmptyTileSuccess() {
        board.setLevel(1);
        board.setUp();
        Tile tile = boardAct.findEmptyTile(0,1, board);
        assertNotNull(tile);
    }

    @Test
    public void testMethodFindEmptyTileFailed() {
        board.setLevel(1);
        board.setUp();
        Tile tile = boardAct.findEmptyTile(0,0, board);
        assertNull(tile);

        tile = boardAct.findEmptyTile(1,1, board);
        assertNull(tile);
    }

    @Test
    public void testGameIsFinished() {
        board.setLevel(1);
        board.setUp();

        board.setTilesGrid(boardAct.swap2Tiles(0, 1, board));
        board.setTilesGrid(boardAct.swap2Tiles(0, 0, board));

        assertTrue(boardAct.isFinished(board));
    }

    @Test
    public void testGameIsNotFinished() {
        board.setLevel(1);
        board.setUp();
        Tile[][] tilesGrid;

        tilesGrid = boardAct.swap2Tiles(0, 1, board);
        board.setTilesGrid(tilesGrid);

        assertFalse(boardAct.isFinished(board));
    }
}
