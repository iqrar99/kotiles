package com.kotiles.project.core.gameplay;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TileTest {
    Tile tile;

    @BeforeEach
    public void setUp() {
        tile = new Tile();
    }

    @Test
    public void testMethodSetAndGetTileId() {
        tile.setTileId(1);
        assertEquals(1, tile.getTileId());
    }

    @Test
    public void testMethodSetAndGetPosition() {
        tile.setRowPosition(1);
        tile.setColPosition(1);
        assertEquals(1, tile.getRowPosition());
        assertEquals(1, tile.getColPosition());
    }

    @Test
    public void testDuplicate() {
        tile.setTileId(1);
        tile.setRowPosition(1);
        tile.setColPosition(1);

        Tile newTile = tile.duplicate();
        assertEquals(1, newTile.getTileId());
        assertEquals(1, newTile.getRowPosition());
        assertEquals(1, newTile.getColPosition());
        assertNotSame(tile, newTile);
    }
}
