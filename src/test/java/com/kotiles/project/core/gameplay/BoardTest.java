package com.kotiles.project.core.gameplay;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.kotiles.project.service.BoardActImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BoardTest {
    Board board;
    BoardActImpl boardAct;

    @BeforeEach
    public void setUp() {
        board = new Board();
        boardAct = new BoardActImpl();
    }

    @Test
    public void testMethodSetAndGetLevel() {
        board.setLevel(1);
        assertEquals(1, board.getLevel());
    }

    @Test
    public void testStart() {
        board.start(2);
        assertEquals(2, board.getLevel());
        assertEquals(4, board.getBoardSize());
    }

    @Test
    public void testMethodSetAndGetBoardSize() {
        board.setLevel(2);
        board.setBoardSize();
        assertEquals(4, board.getBoardSize());
    }

    @Test
    public void testMethodSetAndGetTilesGrid() {
        board.setLevel(1);
        board.setUp();
        Tile[][] tilesGrid = board.getTilesGrid();
        tilesGrid[0][0].setTileId(99);
        board.setTilesGrid(tilesGrid);
        assertEquals(99, board.getTilesGrid()[0][0].getTileId());
    }

    @Test
    public void testIsCompleted() {
        board.setLevel(1);
        board.setUp();
        Tile[][] tilesGrid = board.getTilesGrid();

        board.swapTiles(tilesGrid[0][1]);
        board.swapTiles(tilesGrid[0][0]);

        boolean completed = board.isCompleted();

        assertTrue(completed);
    }

    @Test
    public void testMethodShuffleTheBoard() {
        board.setLevel(1);
        board.setUp();
        Tile[][] tilesGrid = boardAct.shuffle(board);
        boolean isShuffled = false;
        int count = 0;

        outerloop:
        for (int row = 0; row < board.getBoardSize(); row++) {
            for (int col = 0; col < board.getBoardSize(); col++) {
                Tile tmpTile = tilesGrid[row][col];
                if (tmpTile.getTileId() != count) {
                    isShuffled = true;
                    break outerloop;
                } else {
                    count++;
                }
            }
        }

        assertTrue(isShuffled);
    }

    @Test
    public void testMethodSwapTilesSuccess() {
        board.setLevel(2);
        board.setUp();
        board.swapTiles(board.getTilesGrid()[0][1]);
        Tile tileNow = board.getTilesGrid()[0][1];

        assertEquals(0, tileNow.getTileId());
    }

    @Test
    public void testMethodSwapTilesFailed() {
        board.setLevel(1);
        board.setUp();
        board.swapTiles(board.getTilesGrid()[1][1]);

        Tile tileNow = board.getTilesGrid()[1][1];
        assertEquals(4, tileNow.getTileId());

        tileNow = board.getTilesGrid()[0][0];
        assertEquals(0, tileNow.getTileId());
    }
}
