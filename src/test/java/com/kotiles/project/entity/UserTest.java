package com.kotiles.project.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UserTest {
    User user;

    @BeforeEach
    public void setUp() {
        user = new User((long) 123, "Nama Orang", "orang@gmail.com", 0);
    }

    @Test
    public void testGetterMethod() {
        assertEquals(user.getId(), 123);
        assertEquals(user.getName(), "Nama Orang");
        assertEquals(user.getEmail(), "orang@gmail.com");
    }

    @Test
    public void testSetterMethod() {
        user.setName("testname");
        user.setId((long) 2221);
        user.setEmail("test@gmail.com");
        user.setscore(100);

        assertEquals(user.getId(), 2221);
        assertEquals(user.getName(), "testname");
        assertEquals(user.getEmail(), "test@gmail.com");
        assertEquals(user.getscore(), 100);
    }

}
